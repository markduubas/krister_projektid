package Main;

import javafx.scene.paint.Color;

public class GravBall extends Ball {

    public GravBall() {
        super();  //käivita üleval asuv Ball konstruktor
        this.setRadius(40);
        this.setCenterX(300);
        this.setCenterY(300);
        this.setFill(Color.RED);
    }

    @Override
    protected void move() {
        vectorY += 0.5;

        // X telg liikumine
        double praeguneX = this.getCenterX();
        double uusX = praeguneX + vectorX;
        this.setCenterX(uusX);

        // Y telg liikumine
        double praeguneY = this.getCenterY();
        double uusY = praeguneY + vectorY;
        this.setCenterY(uusY);
    }
    @Override
    protected void hide(){
      this.setOpacity(1 - countBounce * 0.1);


    }
}
