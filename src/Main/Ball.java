package Main;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import java.util.List;

public abstract class Ball extends Circle implements Mover {
    double vectorX = Math.random() * 3;
    double vectorY = Math.random() * 3;
    int countBounce = 0;

    public Ball() {
        handleClick();

    }
    private void handleClick() {
        this.setOnMouseClicked(event -> {
            Pane pane = (Pane) this.getParent();
            List nodes = pane.getChildren();
            int index = nodes.indexOf(this);
            nodes.remove(index);
        });

    }
    public void update(){
        move();
        bounce();
        hide();

    }
    protected abstract void hide();


    protected abstract void move();


    protected void bounce(){
        Pane pane = (Pane) this.getParent(); //käib toob parentilt ära omaduse
        double paneWith = pane.getWidth() - this.getRadius();
        double paneHeight = pane.getHeight() - this.getRadius();

        double praeguneX = this.getCenterX();
        double uusX = praeguneX + vectorX;

        if (uusX > paneWith || uusX < this.getRadius()) {
            vectorX *= -1;
//            this.setFill(Color.rgb(rand(256), rand(256), rand(256)));
        }

        double praeguneY = this.getCenterY();

        if (praeguneY > paneHeight) {
            vectorY *= -1;
            this.setCenterY(paneHeight);
            countBounce++;
        } else if (praeguneY < this.getRadius()) {
            vectorY *= -1;
            this.setCenterY(this.getRadius());
        }

    }
    private int rand(int range) {
        return (int) (Math.random() * range);

    }
}
