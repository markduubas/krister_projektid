package Main;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class FlyingBall extends Ball {

    public FlyingBall() {
        super();
        this.setRadius(50);
        this.setCenterX(500);
        this.setCenterY(300);
        this.setFill(Color.BLACK);
    }

    @Override
    protected void move() {
        // X telg liikumine
        double praeguneX = this.getCenterX();
        double uusX = praeguneX + vectorX;
        this.setCenterX(uusX);

        // Y telg liikumine
        double praeguneY = this.getCenterY();
        double uusY = praeguneY + vectorY;
        this.setCenterY(uusY);
    }

    @Override
    protected void hide() {
        double x = this.getCenterX();
        Pane pane = (Pane) this.getParent();
        double paneWidth = pane.getWidth();
        double currentOpacity = this.getOpacity();

//        if (x < paneWidth /2) {
//            this.setOpacity(currentOpacity - 0.01);
        if (x < paneWidth /2) {
            double newOpacity = Math.min(1, currentOpacity + 0.1);
            this.setOpacity(newOpacity);
        }else {
            double newOpacity = Math.max(0, currentOpacity - 0.1);
            this.setOpacity(newOpacity);


        }
    }
}
