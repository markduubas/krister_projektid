package Main;

import com.sun.javafx.animation.TickCalculation;
import javafx.animation.AnimationTimer;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class BoxBall extends Ball {
    double vectorX = 0;
    double vectorY = 5;

    public BoxBall() {
        super();
        this.setRadius(30);
        this.setCenterX(100);
        this.setCenterY(100);
        this.setFill(Color.INDIANRED);
//        Pane pane = (Pane) this.getParent();
//        double paneWith = pane.getWidth() - this.getRadius();
    }

    @Override
    protected void move() {

        double x = this.getCenterX();
        double y = this.getCenterY();

        if (x == 100 && y == 500) {
            vectorX = 5;
            vectorY = 0;
        } else if (x == 500 && y == 500) {
            vectorX = 0;
            vectorY = -5;
        } else if (x == 500 && y == 100) {
            vectorX = -5;
            vectorY = 0;
        } else if (x == 100 && y == 100) {
            vectorX = 0;
            vectorY = 5;
        }
        this.setCenterX(x + vectorX);
        this.setCenterY(y + vectorY);
    }
    @Override
    protected void hide(){
        this.setOpacity(this.getOpacity() - 0.01);

    }

}
