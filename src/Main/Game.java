package Main;

import javafx.animation.AnimationTimer;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Game {
    int stageWidth;
    int stageHeight;
    Pane root = new Pane();
    Stage stage = new Stage();

    public Game(int width, int height) {
        stageWidth = width;
        stageHeight = height;
        createStage();
        createBalls();
        startGameLoop();  //tick  1.frame

    }

    private void startGameLoop() {
        AnimationTimer loop = new AnimationTimer() {
            @Override
            public void handle(long l) {
                root.getChildren().forEach(node -> {
                    Ball ball = (Ball) node;
                    ball.update();
                });
                if (root.getChildren().size() == 0) {
                    Circle gg = new Circle(100, Color.RED);
                    StackPane sp = new StackPane();
                    sp.getChildren().add(gg);
                    stage.setScene(new Scene(sp, stageWidth, stageHeight));
                }
            }
        };
        loop.start();
    }


    private void createBalls() {
        for (int i = 0; i < 5; i++) {
            FlyingBall ball = new FlyingBall();
            root.getChildren().add(ball);
            GravBall gball = new GravBall();
            root.getChildren().add(gball);
            BoxBall bball = new BoxBall();
            root.getChildren().add(bball);

        }
    }

    private void createStage() {
        Stage stage = new Stage();  // aken
        stage.setScene(new Scene(root, stageWidth, stageHeight)); //loo stseen ja ühenda aknaga
        stage.setTitle("PalliMäng");
        stage.show();

    }
}
